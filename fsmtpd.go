package main

import (
	"bytes"
	"log"
	"net"
	"net/mail"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/mhale/smtpd"

	"gopkg.in/yaml.v3"
)

type domain struct {
	Domain    string
	Mailboxes []struct {
		Name string
		Addr []string
	}
}

var domainmailboxes = make(map[string]map[string]string)

func mailHandler(origin net.Addr, from string, to []string, data []byte) error {
	msg, err := mail.ReadMessage(bytes.NewReader(data))
	if err != nil {
		log.Println(err)
		return err
	}
	subject := msg.Header.Get("Subject")
	for _, t := range to {
		mailOK := false
		t := strings.ToLower(t)
		for dom, mailboxes := range domainmailboxes {
			if strings.HasSuffix(t, dom) {
				k := strings.TrimSuffix(t, dom)
				if mb, ok := mailboxes[k]; ok {
					_ = os.Mkdir(mb, 0777)
					ti := time.Now().Unix() & 0x7FFF_FFFF
					fn := filepath.Join(mb, strconv.Itoa(int(ti))+".msg")
					if err = os.WriteFile(fn, data, 0666); err == nil {
						log.Printf("%v bytes from %q for %v with subject %q written to %s", len(data), from, t, subject, fn)
						mailOK = true
					} else {
						log.Printf("%v bytes from %q for %v with subject %q: %v", len(data), from, t, subject, err)
					}
				}
			}
		}
		if !mailOK {
			log.Printf("%v bytes from %q for %v with subject %q ignored", len(data), from, t, subject)	
		}
	}
	return nil
}

func main() {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalln(err)
	}

	cfg, err := os.ReadFile("fsmtpd.yaml")
	if err != nil {
		log.Fatalln(err)
	}

	var domains []domain
	err = yaml.Unmarshal(cfg, &domains)
	if err != nil {
		log.Fatalln(err)
	}

	nErr := 0
	for _, d := range domains {
		if !strings.HasPrefix(d.Domain, "@") {
			log.Println("mail domain name must start with @:", d.Domain)
			nErr++
			continue
		}
		log.Println("Domain:", d.Domain)
		var m = make(map[string]string)
		domainmailboxes[d.Domain] = m
		for _, mb := range d.Mailboxes {
			log.Println("->", mb.Name)
			for _, ad := range mb.Addr {
				m[ad] = mb.Name
				log.Println("    ", ad+d.Domain)
			}
		}
	}

	if nErr > 0 {
		log.Fatalln("found", nErr, "error(s) in fsmtpd.yaml")
	}

	log.Println("This is fsmtpd, listening for SMTP on port 25 ...")
	smtpd.ListenAndServe(":25", mailHandler, "fsmtpd", hostname)
}
